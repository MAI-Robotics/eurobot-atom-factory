#ifndef _ROS_turtlesim_Pose_h
#define _ROS_turtlesim_Pose_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"

namespace custom
{

  class Joystick : public ros::Msg
  {
    public:
      typedef float _x_type;
      _x_type x;
      typedef float _y_type;
      _y_type y;
      typedef int _z_button_type;
      _z_button_type z_button;
      typedef int _c_button_type;
      _c_button_type c_button;

    Joystick():
      x(0),
      y(0),
      z_button(false),
      c_button(false)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      union {
        float real;
        uint32_t base;
      } u_x;
      u_x.real = this->x;
      *(outbuffer + offset + 0) = (u_x.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_x.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_x.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_x.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->x);
      union {
        float real;
        uint32_t base;
      } u_y;
      u_y.real = this->y;
      *(outbuffer + offset + 0) = (u_y.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_y.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_y.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_y.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->y);
      union {
        float real;
        uint32_t base;
      } u_z_button;
      u_z_button.real = this->z_button;
      *(outbuffer + offset + 0) = (u_z_button.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_z_button.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_z_button.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_z_button.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->z_button);
      union {
        float real;
        uint32_t base;
      } u_c_button;
      u_c_button.real = this->c_button;
      *(outbuffer + offset + 0) = (u_c_button.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_c_button.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_c_button.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_c_button.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->c_button);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      union {
        float real;
        uint32_t base;
      } u_x;
      u_x.base = 0;
      u_x.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_x.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_x.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_x.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->x = u_x.real;
      offset += sizeof(this->x);
      union {
        float real;
        uint32_t base;
      } u_y;
      u_y.base = 0;
      u_y.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_y.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_y.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_y.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->y = u_y.real;
      offset += sizeof(this->y);
      union {
        float real;
        uint32_t base;
      } u_z_button;
      u_z_button.base = 0;
      u_z_button.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_z_button.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_z_button.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_z_button.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->z_button = u_z_button.real;
      offset += sizeof(this->z_button);
      union {
        float real;
        uint32_t base;
      } u_c_button;
      u_c_button.base = 0;
      u_c_button.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_c_button.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_c_button.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_c_button.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->c_button = u_c_button.real;
      offset += sizeof(this->c_button);
     return offset;
    }

    const char * getType(){ return "custom/Joystick"; };
    const char * getMD5(){ return "863b248d5016ca62ea2e895ae5265cf9"; };

  };

}
#endif