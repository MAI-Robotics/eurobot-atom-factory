#include <Arduino.h>
#include <Wire.h>
#include "Nunchuk.h"
#include <ros.h>

ros::NodeHandle nh;

void setup () {
  nh.initNode();

  Wire.begin();
  Wire.setClock(400000);
  nunchuk_init();

  while (!nh.connected())
  {
    nh.spinOnce();
  }

  nh.loginfo("Controller started and connected");
}

void loop () {
  if (nunchuk_read()) {
  }
}