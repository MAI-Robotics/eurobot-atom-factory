#ifndef SENSORS_H
#define SENSORS_H

#include <mbed.h>
#include <maiMath.h>
#include <Ultrasonic.h>
#include <rtos.h>

extern Serial pc;
extern bool gameRunning;
extern bool pauseMeasure;

#define FLEcho D10
#define FREcho PC_9
#define BREcho D3
#define BLEcho PH_1

#define FLTrig D8
#define FRTrig PB_8
#define BRTrig D2
#define BLTrig D7

// Thread that reads the sensors continously
Thread sensorReader;

// Distance variables
// One for each sensor (except the arm) to access the distances
// The median is beeing build out of four measurements 
int distFL;
int distFR;
int distBL;
int distBR;

// Save wether we are currently measuring
bool measuring = false;

void updateFL(int dist) {
  distFL = dist;
}

void updateFR(int dist) {
  distFR = dist;
}

void updateBL(int dist) {
  distBL = dist;
}

void updateBR(int dist) {
  distBR = dist;
  //printf("BR %d", dist);
}

ultrasonic FL(FLTrig, FLEcho, 0.2, 0.2, &updateFL);
ultrasonic FR(FRTrig, FREcho, 0.2, 0.2, &updateFR);
ultrasonic BL(BLTrig, BLEcho, 0.2, 0.2, &updateBL);
ultrasonic BR(BRTrig, BREcho, 0.2, 0.2, &updateBR);

// This is what the "sensorReader"-Thread does
void readSensorsCont() {
  while (gameRunning) {
    while (pauseMeasure) {
      ThisThread::sleep_for(1000);
    }

    FL.checkDistance();
    FR.checkDistance();
    BL.checkDistance();
    BR.checkDistance();

    //printf("FL: %4d | FR: %4d | BL: %4d | BR: %4d\n", distFL, distFR, distBL, distBR);
  }
}

void initAllSensors()
{
  printf("Sensors initializing!\n");

  FL.startUpdates();
  FR.startUpdates();
  BL.startUpdates();
  BR.startUpdates();

  printf("All sensors initalized\n");
}

#endif