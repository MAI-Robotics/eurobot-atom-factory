#include <Arduino.h>
#include <SoftwareSerial.h>

#define println(text) {Serial.println(text); debug.println(text);}
#define print(text) {Serial.print(text); debug.print(text);}

#define stepPin 4
#define enablePin 5
#define dirPinLeft 7
#define dirPinRight 6

#define IR 2

#define feedback 9

String inputString = "";
bool stringComplete = false;

String inputStringDeb = "";
bool stringCompleteDeb = false;

const float tPerStep = 840; //minimal delay between the steps in µs
const float tPerSlowStep = tPerStep * 1;

volatile bool interrupted = false;

SoftwareSerial debug(3, 8); //RX, TX connection to the nucleo

void serialEvent()
{
  while (Serial.available())
  {
    char inChar = (char)Serial.read();
    if (inChar == '\n')
    {
      stringComplete = true;
    }
    else
    {
      inputString += inChar;
    }
  }
}

void interruptMe() {
  interrupted = true;
}

void turnFast(unsigned int steps, bool dir)
{
  digitalWrite(feedback, LOW);

  delay(10);

  digitalWrite(dirPinLeft, dir ? 1 : 0);
  digitalWrite(dirPinRight, dir ? 1 : 0);
  digitalWrite(enablePin, LOW);

  delay(10);

  float a = 0; //for acceleration
  for (unsigned int i = 0; i < steps; i++)
  {
    a = 0;
    if (i < 400 && i <= steps / 2)
    {
      a = tPerStep * 0.0025 * (400 - i);
    }

    if (steps - i < 400 && i > steps / 2)
    {
      a = tPerStep * 0.0025 * (400 - (steps - i));
    }

    if (digitalRead(IR) && steps - i > 200) {
      for (int blahh = 0; blahh < 100; blahh++)
      {
        a = tPerStep * 0.0025 * (100 - blahh);

        digitalWrite(stepPin, HIGH);
        delayMicroseconds(tPerStep + a);
        digitalWrite(stepPin, LOW);
        delayMicroseconds(tPerStep + a);
      }

      digitalWrite(enablePin, HIGH);
      while (digitalRead(IR))
      {
        delay(500);
      }

      digitalWrite(enablePin, LOW);

      for (int blubb = 0; blubb < 100; blubb++)
      {
        a = tPerStep * 0.0025 * (100 - blubb);

        digitalWrite(stepPin, HIGH);
        delayMicroseconds(tPerStep + a);
        digitalWrite(stepPin, LOW);
        delayMicroseconds(tPerStep + a);
      }

      i += 200;
    }

    digitalWrite(stepPin, HIGH);
    delayMicroseconds(tPerStep + a);
    digitalWrite(stepPin, LOW);
    delayMicroseconds(tPerStep + a);
  }

  digitalWrite(enablePin, HIGH);

  digitalWrite(feedback, HIGH);
}

void driveFast(unsigned int steps, bool dir)
{
  digitalWrite(feedback, LOW);

  delay(10);

  digitalWrite(dirPinLeft, dir ? 1 : 0);
  digitalWrite(dirPinRight, dir ? 0 : 1);
  digitalWrite(enablePin, LOW);

  delay(10);

  float a = 0; //for acceleration
  for (unsigned int i = 0; i < steps; i++)
  {
    a = 0;
    if (i < 400 && i <= steps / 2)
    {
      a = tPerStep * 0.0025 * (400 - i);
    }

    if (steps - i < 400 && i > steps / 2)
    {
      a = tPerStep * 0.0025 * (400 - (steps - i));
    }

    if (digitalRead(IR) && steps - i > 200) {
      for (int blahh = 0; blahh < 100; blahh++)
      {
        a = tPerStep * 0.0025 * (100 - blahh);

        digitalWrite(stepPin, HIGH);
        delayMicroseconds(tPerStep + a);
        digitalWrite(stepPin, LOW);
        delayMicroseconds(tPerStep + a);
      }

      digitalWrite(enablePin, HIGH);
      while (digitalRead(IR))
      {
        delay(500);
      }
      digitalWrite(enablePin, LOW);

      for (int blubb = 0; blubb < 100; blubb++)
      {
        a = tPerStep * 0.0025 * (100 - blubb);

        digitalWrite(stepPin, HIGH);
        delayMicroseconds(tPerStep + a);
        digitalWrite(stepPin, LOW);
        delayMicroseconds(tPerStep + a);
      }

      i += 200;
    }

    digitalWrite(stepPin, HIGH);
    delayMicroseconds(tPerStep + a);
    digitalWrite(stepPin, LOW);
    delayMicroseconds(tPerStep + a);
  }

  digitalWrite(enablePin, HIGH);

  digitalWrite(feedback, HIGH);
}

void driveSlow(unsigned int steps, bool dir)
{
  digitalWrite(feedback, LOW);

  delay(10);

  digitalWrite(dirPinLeft, dir ? 1 : 0);
  digitalWrite(dirPinRight, dir ? 0 : 1);
  digitalWrite(enablePin, LOW);

  delay(10);

  float a = 0; //for acceleration
  for (unsigned int i = 0; i < steps; i++)
  {
    a = 0;
    if (i < 400 && i <= steps / 2)
    {
      a = tPerSlowStep * 0.0025 * (400 - i);
    }

    if (steps - i < 400 && i > steps / 2)
    {
      a = tPerSlowStep * 0.0025 * (400 - (steps - i));
    }

    if (digitalRead(IR) && steps - i > 200) {
      for (int blahh = 0; blahh < 100; blahh++)
      {
        a = tPerSlowStep * 0.0025 * (100 - blahh);

        digitalWrite(stepPin, HIGH);
        delayMicroseconds(tPerSlowStep + a);
        digitalWrite(stepPin, LOW);
        delayMicroseconds(tPerSlowStep + a);
      }

      digitalWrite(enablePin, HIGH);
      while (digitalRead(IR))
      {
        delay(500);
      }
      digitalWrite(enablePin, LOW);

      for (int blubb = 0; blubb < 100; blubb++)
      {
        a = tPerSlowStep * 0.0025 * (100 - blubb);

        digitalWrite(stepPin, HIGH);
        delayMicroseconds(tPerSlowStep + a);
        digitalWrite(stepPin, LOW);
        delayMicroseconds(tPerSlowStep + a);
      }

      i += 200;
    }

    digitalWrite(stepPin, HIGH);
    delayMicroseconds(tPerSlowStep + a);
    digitalWrite(stepPin, LOW);
    delayMicroseconds(tPerSlowStep + a);
  }

  digitalWrite(enablePin, HIGH);

  digitalWrite(feedback, HIGH);
}

void setup()
{
  Serial.begin(115200);
  debug.begin(115200);
  println("Drivoino online");
  inputString.reserve(200);
  attachInterrupt(digitalPinToInterrupt(IR), interruptMe, RISING);

  pinMode(dirPinLeft, OUTPUT);
  pinMode(dirPinRight, OUTPUT);
  pinMode(enablePin, OUTPUT);
  pinMode(feedback, OUTPUT);
  pinMode(stepPin, OUTPUT);

  digitalWrite(enablePin, HIGH);
  digitalWrite(feedback, HIGH);
}

int stepsDrive = 0;
int stepsTurn = 0;
int stepsSlowDrive = 0;

void loop()
{
  if (stringComplete)
  {
    println(inputString);
    String substr = inputString.substring(inputString.indexOf("!!") + 2);
    if (substr != NULL)
    {
      /**
       * d == drive
       * t == turn
      */
      char command = substr.charAt(0);
      if (command == 'd')
      {
        stepsDrive = substr.substring(substr.indexOf('d') + 1).toInt();
      }
      else if (command == 't')
      {
        stepsTurn = substr.substring(substr.indexOf('t') + 1).toInt();
      }
      else if (command == 's')
      {
        stepsSlowDrive = substr.substring(substr.indexOf('s') + 1).toInt();
      }
    }
    stringComplete = false;
    inputString = "";
  }

  while (debug.available())
  {
    char inChar = (char)debug.read();
    if (inChar == '\n')
    {
      stringCompleteDeb = true;
    }
    else
    {
      inputStringDeb += inChar;
    }
  }

  if (stringCompleteDeb)
  {
    String substr = inputStringDeb.substring(inputString.indexOf("!!") + 2);
    if (substr != NULL)
    {
      //println(substr);
      /**
       * d == drive
       * t == turn
      */
      char command = substr.charAt(0);
      if (command == 'd')
      {
        stepsDrive = substr.substring(substr.indexOf('d') + 1).toInt();
      }
      else if (command == 't')
      {
        stepsTurn = substr.substring(substr.indexOf('t') + 1).toInt();
      }
    }
    stringCompleteDeb = false;
    inputStringDeb = "";
  }

  if (stepsDrive != 0) {
    driveFast(abs(stepsDrive), stepsDrive > 0);
    stepsDrive = 0;
  }

  if (stepsTurn != 0)
  {
    turnFast(abs(stepsTurn), stepsTurn > 0);
    stepsTurn = 0;
  }

  if (stepsSlowDrive != 0) {
    driveSlow(abs(stepsSlowDrive), stepsSlowDrive > 0);
    stepsSlowDrive = 0;
  }
  
}
